const express = require('express');
const app = express();
const mustacheExpress = require('mustache-express');
const bodyParser = require('body-parser');
const pool = require('./db')
const pgCamelCase = require('pg-camelcase');
var revertCamelCase = pgCamelCase.inject(require('pg'));

// pgCamelCase.inject(require('pg'));
require('dotenv').config();

revertCamelCase();

const mustache = mustacheExpress();
mustache.cache = null;
app.engine('mustache', mustache);
app.set('view engine', 'mustache');

app.use(bodyParser.urlencoded({extended : false}));
app.use(express.static('public'));

app.get('/books', (req, res) => {

    pool.connect()
        .then(() => {
            console.log('Koneksi berhasil');
            //query
            return pool.query('SELECT * FROM books');
        })
        .then((result) => {
            console.log('result?', result);
            res.render('book-list', {
                books: result.rows
            });
        })
        .catch((err) => {
            console.log('err', err);
            res.send('Terjadi Kesalahan');
        });
});

app.get('/book/add', (req,res) =>{
    res.render('book-form');
})

app.post('/book/add', (req,res) =>{
    console.log('post body', req.body);

    pool.connect()
        .then(() => {
            console.log('Koneksi berhasil');
            //query
            const sql = 'INSERT INTO books (judul, authors) VALUES ($1, $2)'
            const params = [req.body.judul, req.body.authors]
            return pool.query(sql, params);
        })
        .then((result) => {
            console.log('result?', result);
            res.redirect('/books');
        })
        .catch((err) => {
            console.log('err', err);
            res.redirect('/books');
        });
});

app.post('/book/delete/:id', (req,res) => {
    console.log('Deleting id', req.params.id);

    pool.connect()
        .then(() => {
            console.log('Koneksi berhasil');
            //query
            return pool.query(`DELETE FROM books WHERE id_buku = ${req.params.id}`);
        })
        .then((result) => {
            console.log('result?', result);
            res.redirect('/books');
        })
        .catch((err) => {
            console.log('err', err);
            res.send('Terjadi Kesalahan');
        });
});

app.get('/book/edit/:id', (req,res)=> {
    pool.connect()
        .then(() => {
            //query
            const sql = ' '
            const params = [req.body.judul, req.body.authors]
            return pool.query(`SELECT * FROM books WHERE id_buku = ${req.params.id}`);
        })
        .then((result) => {

            if(result.rowCount === 0) {
                res.redirect('/books');
                return;
            }

            // console.log('result?', result);
            res.render('book-edit', {
                book: result.rows[0]
            });
        })
        .catch((err) => {
            console.log('err', err);
            res.redirect('/books');
        });
});

app.post('/book/edit/:id', (req,res) => {
    pool.connect()
        .then(() => {
            const sql = 'UPDATE books SET judul = $1, authors = $2 WHERE id_buku = $3';
            const params = [req.body.judul, req.body.authors, req.params.id];

            return pool.query(sql,params);
        })
        .then((result) => {
            console.log('Update result', result);
            res.redirect('/books');
        })
        .catch((err) => {
            console.log('Update err', err);
            res.redirect('/books')
        });
})

app.listen(process.env.PORT, () => {
    console.log(`Listening on port ${process.env.PORT}`);
})